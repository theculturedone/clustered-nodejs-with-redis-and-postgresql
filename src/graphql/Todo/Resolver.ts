import { Args, Ctx, Mutation, Query, Resolver } from "type-graphql"
import { Todo } from "./ObjectType"
import { TodoService } from "./Service";

@Resolver(Todo)
export class TodoResolver {
  // => Gets all issues
  @Query(() => [Todo], { description: 'Gets all todos' })
  async todos(): Promise<Todo[]> {
    return await TodoService.getMany({
      include: {
        user: true
      }
    });
  }
}
