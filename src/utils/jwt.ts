import jwt from "jsonwebtoken"
import { User } from "@prisma/client"

// => Generates a new access token
export const generateAccessToken = (user: User) => {
  return jwt.sign({ userId: user.id }, process.env.JWT_ACCESS_SECRET, {
    expiresIn: "1h",
  })
}

// => Generates a new refresh token
export const generateRefreshToken = (user: User, jti) => {
  return jwt.sign({
    userId: user.id,
    jti
  }, process.env.JWT_REFRESH_SECRET, {
    expiresIn: "15days",
  })
}

// => Generate both tokens
export const generateTokens = (user: User, jti) => {
  const accessToken = generateAccessToken(user)
  const refreshToken = generateRefreshToken(user, jti)
  return {
    accessToken,
    refreshToken,
  }
}