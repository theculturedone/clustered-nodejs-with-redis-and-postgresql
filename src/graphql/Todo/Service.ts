import { prisma } from "../../prisma/client"

import { Todo, Prisma } from '@prisma/client'


export const TodoService = {

  getOneOrFail: async (
    whereUniqueInput: Prisma.TodoWhereUniqueInput,
  ): Promise<Todo | null> => {
    return await prisma.todo.findUniqueOrThrow({
      where: whereUniqueInput,
    })
  },

  getOne: async (
    whereUniqueInput: Prisma.TodoWhereUniqueInput,
  ): Promise<Todo | null> => {
    return await prisma.todo.findUnique({
      where: whereUniqueInput,
    })
  },

  getMany: async (
    params: {
      skip?: number
      take?: number
      cursor?: Prisma.TodoWhereUniqueInput
      where?: Prisma.TodoWhereInput
      orderBy?: Prisma.TodoOrderByWithRelationInput,
      include?: Prisma.TodoInclude
  }): Promise<Todo[]> => {
    const { skip, take, cursor, where, orderBy, include } = params
    return await prisma.todo.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
      include
    });
  },

  createOne: async (
    params: { 
      data: Prisma.TodoCreateInput
    }): Promise<Todo> => {
    const { data } = params
    return await prisma.todo.create({
      data: data,
    })
  },

  updateOne: async (
    params: {
      where: Prisma.TodoWhereUniqueInput
      data: Prisma.TodoUpdateInput
  }): Promise<Todo> => {
    const { where, data } = params
    return await prisma.todo.update({
      data,
      where,
    })
  },

  deleteOne: async (
    where: Prisma.TodoWhereUniqueInput
    ): Promise<Todo> => {
    return await prisma.todo.delete({
      where,
    })
  }

}  
