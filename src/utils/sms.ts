import { SNSClient, PublishCommand } from "@aws-sdk/client-sns"
import console from "chalk-console"
const client = new SNSClient({ region: process.env.AWS_REGION })

export const sendCodeViaSms = async (phoneNumber, code) => {
  const params = {
    Message: "Your code is: " + code /* required */,
    PhoneNumber: phoneNumber, //PHONE_NUMBER, in the E.164 phone number structure
  }
  if(process.env.ENVIRONMENT === "PROD") {
  // eslint-disable-next-line no-useless-catch
  try {
    const data = await client.send(new PublishCommand(params))
    console.log("Successfully sent sms.")
    return data // For unit tests.
  } catch (err) {
    console.error("Something went wrong while attempting to send sms.")
    console.error(err)
  }
  }
}
