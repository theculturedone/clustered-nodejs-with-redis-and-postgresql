import { ApolloServer } from "apollo-server-express"
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core"
import { ApolloServerPluginCacheControl } from 'apollo-server-core';
import express from "express"
import http from "http"
import "reflect-metadata"
import { schema } from "./graphql/schema"
import { prisma } from "./prisma/client"
import GraphQLContext from "./graphql/context"
import { getUserFromToken } from "./utils/getUserFromToken"
import cron from "node-cron"
import AWS from "aws-sdk"
import cronjobs from "./cronjobs"
import { redis } from "./redis/client"
import { BaseRedisCache } from 'apollo-server-cache-redis';

import responseCachePlugin from 'apollo-server-plugin-response-cache';
AWS.config.update({region: process.env.AWS_REGION})

import * as dotenv from "dotenv"
import { applyFixtures } from "./prisma/fixtures"
dotenv.config({ path: __dirname + "../.env" })

export async function bootstrap(schema, httpServer) {
  // Required logic for integrating with Express
  const app = express()

  // Same ApolloServer initialization as before, plus the drain plugin.
  const server = new ApolloServer({
    schema,
    cache: new BaseRedisCache({
      client: redis,
    }),
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }), 
      responseCachePlugin(), 
      ApolloServerPluginCacheControl({ defaultMaxAge: 5 }) 
    ],
    context: async ({ req }) => {
      // => Get the user token from the headers.
      const token = req.headers.authorization || ""

      // =>  Try to retrieve a user with the token
      const user = await getUserFromToken(token)

      // => If we have roles in our app and we cannot have user.role for some reason
      //  we can then just compute the role here and add it to our context for guards to check
      // => Add the user to the context
      return { 
        user,
        prisma,
        token
      } as GraphQLContext
    },
  })

  // More required logic for integrating with Express
  await server.start()
  server.applyMiddleware({
    app,

    // By default, apollo-server hosts its GraphQL endpoint at the
    // server root. However, *other* Apollo Server packages host it at
    // /graphql. Optionally provide this to match apollo-server.

    // TODO: add validation pipeline for class validator here (Look at nestjs example )

    path: "/"
  })

  // Modified server startup
  await new Promise<void>(resolve => httpServer.listen({ port: 4000 }, resolve))

  // Cronjobs scheduling
  process.env.ENVIRONMENT === "PROD" && cronjobs.forEach((cronjob) => {
    cron.schedule(cronjob.crontab, cronjob.job)
  })

  await applyFixtures(prisma)

  httpServer.on('request', app);
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
}

const httpServer = http.createServer()
bootstrap(schema, httpServer)

// HMR configuration

if (module.hot) {
  module.hot.accept( () => {
    console.info('Reloading HTTP server');
    httpServer.removeAllListeners('request')
    bootstrap(schema, httpServer)
  });
  module.hot.dispose(() => {
    httpServer.close()
  })
}