
import { printSchema } from "graphql"
import "reflect-metadata"
import "source-map-support/register"
import { schema } from "../schema"

console.log(printSchema(schema))