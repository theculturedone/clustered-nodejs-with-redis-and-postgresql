import { GraphQLScalarType } from "graphql"
import { Field, ID, ObjectType, } from "type-graphql"
import { User } from "../User/ObjectType"

@ObjectType("Todo", {description: "Simple Todo model"})
export class Todo {
  @Field(() => ID)
    id: string

  @Field(() => User, { nullable: true, description: "The user who created this todo"})
    user?: User 

  @Field(() => Boolean, { description: "Todo can be done or not (true or false)"})
    done: boolean

  @Field(() => Date, { nullable: true })
    created_at?: Date 

  @Field(() => Date, { nullable: true })
    updated_at?: Date
}