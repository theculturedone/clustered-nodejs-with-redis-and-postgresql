
import Redis from 'ioredis'
import console from "chalk-console"

const connectToRedis = () => {
  const redis = new Redis({
    host: process.env.REDIS_HOST,
    port: +process.env.REDIS_PORT, // string to number conversion with +string
    password: process.env.REDIS_PASSWORD
  })
  // Notifications for redis connection
  redis.on('connect', () => console.info('Successfully connected to Redis'))
  redis.on('error', err => console.error(err))
  return redis 
}
export const redis = connectToRedis()
