import { User } from "../graphql/User/ObjectType"
import jwt, { JwtPayload } from "jsonwebtoken"
import { prisma } from "../prisma/client"

export const getUserFromToken = async (token: string): Promise<User> => {
  let jwt_decoded: string | JwtPayload = null
  jwt.verify(token, process.env.JWT_ACCESS_SECRET, (err, decoded): string | JwtPayload => {
    if (err) {
      return null
    }
    jwt_decoded = decoded
    return decoded as JwtPayload
  })
  const user = (jwt_decoded as JwtPayload)?.userId && await prisma.user.findFirst({
    where: {
      id: (jwt_decoded as JwtPayload)?.userId
    }
  })
  return user
}