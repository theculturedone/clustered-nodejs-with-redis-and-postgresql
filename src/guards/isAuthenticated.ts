import { PrismaClient } from "@prisma/client"
import GraphQLContext from "../graphql/context"
import { Guard } from "./guardType"

export const isAuthenticated: Guard = {
  func: async (ctx: GraphQLContext, prisma: PrismaClient) => {

    return true
  },
  errorMsg: "You need to be logged in to perform this action. \n"
}