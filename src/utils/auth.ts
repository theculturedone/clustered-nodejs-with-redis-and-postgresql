import { prisma } from "../prisma/client"
import { hashToken } from "./hashToken"

// used when we create a refresh token.
export const addRefreshTokenToWhitelist = ({ jti, refreshToken, userId }) => {
  return prisma.refreshToken.create({
    data: {
      id: jti,
      hashedToken: hashToken(refreshToken),
      userId
    },
  })
}

// used to check if the token sent by the client is in the database.
export const findRefreshTokenById = (id) => {
  return prisma.refreshToken.findUnique({
    where: {
      id,
    },
  })
}

// soft delete tokens after usage.
export const deleteRefreshToken = (id) => {
  return prisma.refreshToken.update({
    where: {
      id,
    },
    data: {
      revoked: true
    }
  })
}

export const revokeTokens = (userId) => {
  return prisma.refreshToken.updateMany({
    where: {
      userId
    },
    data: {
      revoked: true
    }
  })
}
