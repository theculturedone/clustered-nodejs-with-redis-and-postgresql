import { PrismaClient } from "@prisma/client"
import console from "chalk-console"
// TODO: add primsa-redis-middleware here and also install it
// Allows us to log prisma errors when initializing the client
const getPrismaClient = () => {
  try {
    const prismaClient = new PrismaClient()
    return prismaClient
  } catch (e) {
    console.error("Something went wrong while initializing prismaClient")
    console.error(e)
  }
}

export const prisma = getPrismaClient()