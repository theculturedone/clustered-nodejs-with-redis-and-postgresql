import { sendCodeViaSms } from "./sms"
import { prisma } from "../prisma/client"

// Hardcoded dev numbers
const devNumbers = [
  "+40733989550",
]

interface LoginAttempt {
    phoneNumber: string;
    otp: string;
    expiresAt: Date;
}

interface userDataInput {
  firstName: string;
  lastName: string;
  phoneNumber: string;
}
interface RegisterAttempt {
  otp: string;
  userToCreate: userDataInput;
  expiresAt: Date;
}

interface PhoneNumberChangeAttempt {
  otp: string;
  phoneNumber: string;
  userId: string;
  expiresAt: Date;
}

const OTP_LIFETIME = 10 // in minutes for now its hardcorded here
const loginAttempts: LoginAttempt[] = []
const registerAttempts: RegisterAttempt[] = []
const phoneNumberChangeAttempts: PhoneNumberChangeAttempt[] = []
// TODO: Move all this into redis cache so we can test that it actually works as a ram sharing mechanism

// Function to generate OTP
export const generateOTP = (): string => {
  // Declare a digits variable 
  // which stores all digits
  const digits = "0123456789"
  let OTP = ""
  for (let i = 0; i < 6; i++ ) {
    OTP += digits[Math.floor(Math.random() * 10)]
  }
  console.log("otp: ", OTP)
  return OTP
}

// Attempt to find valid login attempt matching the provided otp
export const getLoginAttempt = (otp: string): LoginAttempt => {
  let foundValidAttempt = null
  loginAttempts.map((attempt, index) => {
    if(attempt?.otp === otp && attempt?.expiresAt > new Date()) {
      foundValidAttempt = attempt
      loginAttempts[index] = null
    }
  })
  return foundValidAttempt
}

// Attempt to find valid register attempt matching the provided otp
export const getRegisterAttempt = (otp: string): RegisterAttempt => {
  let foundValidAttempt = null
  registerAttempts.map((attempt, index) => {
    if(attempt?.otp === otp && attempt?.expiresAt > new Date()) {
      foundValidAttempt = attempt
      registerAttempts[index] = null
    }
  })
  return foundValidAttempt
}

// Attempt to find phone number change attempt matching the provided otp
export const getPhoneNumberChangeAttempt = (otp: string): PhoneNumberChangeAttempt => {
  let foundValidAttempt = null
  phoneNumberChangeAttempts.map((attempt, index) => {
    if(attempt?.otp === otp && attempt?.expiresAt > new Date()) {
      foundValidAttempt = attempt
      phoneNumberChangeAttempts[index] = null
    }
  })
  return foundValidAttempt
}

// Create new login attempt with the user's phone number and pushes it to the loginAttempts array
export const pushLoginAttempt = async (phoneNumber: string): Promise<void> => {
  const otp = generateOTP()

  let isDevPhoneNumber = false
  devNumbers.map((devNumber) => {
    if(devNumber === phoneNumber) {
      isDevPhoneNumber = true
    }
  })
  loginAttempts.push({
    phoneNumber: phoneNumber,
    otp: isDevPhoneNumber ? "666666" : otp,
    expiresAt: new Date(new Date().getTime() + OTP_LIFETIME * 60000)
  })
  // we attempt to send the code to the user via AWS SNS
  await sendCodeViaSms(phoneNumber, otp)
}

// Same but with registration
export const pushRegisterAttempt = async (userData: userDataInput): Promise<void> => {
  const otp = generateOTP()
  
  let isDevPhoneNumber = false
  devNumbers.map((devNumber) => {
    if(devNumber === userData.phoneNumber) {
      isDevPhoneNumber = true
    }
  })

  registerAttempts.push({
    otp: isDevPhoneNumber ? "666666" : otp,
    userToCreate: userData,
    expiresAt: new Date(new Date().getTime() + OTP_LIFETIME * 60000)
  })
  // we attempt to send the code to the user via AWS SNS
  await sendCodeViaSms(userData.phoneNumber, otp)
}

// Same but with phone number change attempt
export const pushPhoneNumberChangeAttempt = async (newPhoneNumber: string, userId: string): Promise<void> => {
  const otp = generateOTP()
  
  let isDevPhoneNumber = false
  devNumbers.map((devNumber) => {
    if(devNumber === newPhoneNumber) {
      isDevPhoneNumber = true
    }
  })

  phoneNumberChangeAttempts.push({
    otp: isDevPhoneNumber ? "666666" : otp,
    phoneNumber: newPhoneNumber,
    userId: userId,
    expiresAt: new Date(new Date().getTime() + OTP_LIFETIME * 60000)
  })
  
  // we attempt to send the code to the user via AWS SNS
  await sendCodeViaSms(newPhoneNumber, otp)
}

