import { Args, Ctx, Mutation, Query, Resolver } from "type-graphql"
import { User } from "./ObjectType"
import { UserService } from "./Service";

@Resolver(User)
export class UserResolver {
  // => Gets all issues
  @Query(() => [User], { description: 'Gets all users' })
  async todos(): Promise<User[]> {
    return await UserService.getMany({});
  }
}
