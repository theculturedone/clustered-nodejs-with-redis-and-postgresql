How to run: 
  - start docker desktop
  - npm run db:start
  - npm run prisma:push
  - npm run dev
  - localhost:4000
![proposedBackendImg](proposedBackend.png) 

Notes:
  - prisma-redis-middleware was removed because of date time serializing issue with graphql. I will attempt to cache at response level instead. Redis will continue to be used as a common store for all the instances for example for login attempts.
  - 
I will attempt to implement this backend architecture. Possibly with deploy target => Lambdas.

