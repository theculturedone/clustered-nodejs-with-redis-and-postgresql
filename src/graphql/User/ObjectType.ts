import { Field, ID, ObjectType } from "type-graphql"

@ObjectType("User", { description: "Simple User model"})
export class User {
  @Field(() => ID)
    id: string

  @Field(() => String, { description: "User's first name" })
    firstName: string

  @Field(() => String, { description: "User's last name"})
    lastName: string

  @Field(() => String, { description: "User's phone number. (Must follow E.164 format)"})
    phoneNumber: string

  @Field(() => Date, { nullable: true })
    created_at?: Date

  @Field(() => Date, { nullable: true })
    updated_at?: Date
}