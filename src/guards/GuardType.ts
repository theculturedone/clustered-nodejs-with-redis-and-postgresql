import { PrismaClient } from "@prisma/client";
import GraphQLContext from "../graphql/context";

export interface Guard {
  func: (ctx: GraphQLContext, prisma: PrismaClient) => Promise<boolean>,
  errorMsg: string
}