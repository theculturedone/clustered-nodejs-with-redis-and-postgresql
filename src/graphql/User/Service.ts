import { prisma } from "../../prisma/client"

import { User, Prisma } from '@prisma/client'


export const UserService = {

  getOneOrFail: async (
    whereUniqueInput: Prisma.UserWhereUniqueInput,
  ): Promise<User | null> => {
    return await prisma.user.findUniqueOrThrow({
      where: whereUniqueInput,
    })
  },

  getOne: async (
    whereUniqueInput: Prisma.UserWhereUniqueInput,
  ): Promise<User | null> => {
    return await prisma.user.findUnique({
      where: whereUniqueInput,
    })
  },

  getMany: async (
    params: {
      skip?: number
      take?: number
      cursor?: Prisma.UserWhereUniqueInput
      where?: Prisma.UserWhereInput
      orderBy?: Prisma.UserOrderByWithRelationInput
  }): Promise<User[]> => {
    const { skip, take, cursor, where, orderBy } = params
    return await prisma.user.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
    });
  },

  createOne: async (
    params: { 
      data: Prisma.UserCreateInput
    }): Promise<User> => {
    const { data } = params
    return await prisma.user.create({
      data: data,
    });
  },

  updateOne: async (
    params: {
      where: Prisma.UserWhereUniqueInput
      data: Prisma.UserUpdateInput
  }): Promise<User> => {
    const { where, data } = params
    return await prisma.user.update({
      data,
      where,
    })
  },

  deleteOne: async (
    where: Prisma.UserWhereUniqueInput
    ): Promise<User> => {
    return await prisma.user.delete({
      where,
    })
  }

}  
