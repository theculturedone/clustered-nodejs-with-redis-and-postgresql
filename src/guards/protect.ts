import { PrismaClient } from "@prisma/client"
import GraphQLContext from "../graphql/context"
import { Guard } from "./guardType"

// => protect will be used as the first thing you call inside an endpoint
// => ex: protect(ctx, prisma, isAuthenticated, isAdmin, ...etc) will handle protecting the route
export const protect = async (ctx: GraphQLContext, prisma: PrismaClient, ...args: Array<Guard>) => {
  let errors: Array<string> = []
  for (let i = 0; i < args.length; i++) {
    const isOk = await args[i].func(ctx, prisma)
    if(isOk) {
      errors.push(args[i].errorMsg)
    }
  }
  if(errors.length) {
    throw new Error("UNAUTHORIZED \n [ \n" + errors.join(',\n') + '\n ]')
  }
}