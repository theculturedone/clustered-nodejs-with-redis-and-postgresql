import { PrismaClient } from "@prisma/client"
import { User } from "../User/ObjectType"

// => Context ts type
export default class GraphQLContext {
  prisma: PrismaClient
  user: User
  token: string
}