import { buildSchemaSync, NonEmptyArray } from "type-graphql"
import { applyMiddleware } from "graphql-middleware"
import { UserResolver } from "./User/Resolver"
import { TodoResolver } from "./Todo/Resolver"

export const resolvers = [
  UserResolver,
  TodoResolver
// eslint-disable-next-line @typescript-eslint/no-explicit-any
] as NonEmptyArray<any>

export const schema = applyMiddleware(
  buildSchemaSync({
    resolvers,
  })
)
